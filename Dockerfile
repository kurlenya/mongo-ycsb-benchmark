FROM ubuntu:14.04
ENV YCSB_VERSION=0.12.0
ENV YCSB=/ycsb-${YCSB_VERSION}
ENV PATH=${PATH}:/usr/bin 
RUN apt-get update
RUN apt-get -y install wget python
RUN apt-get install -f
RUN apt-get -y install openjdk-7-jre
RUN wget --quiet https://github.com/brianfrankcooper/YCSB/releases/download/${YCSB_VERSION}/ycsb-${YCSB_VERSION}.tar.gz
RUN tar -zxvf ycsb-${YCSB_VERSION}.tar.gz
RUN rm -rf ycsb-${YCSB_VERSION}.tar.gz
WORKDIR ${YCSB} 
COPY workload ${YCSB}/workloads
ENTRYPOINT ["./bin/ycsb"] 
CMD ["--help"]